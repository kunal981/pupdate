//
//  GalleryViewController.m
//  pUpdate
//
//  Created by brst on 5/27/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import "GalleryViewController.h"
#import "CollectionViewCell.h"

@interface GalleryViewController ()<UICollectionViewDataSource,UICollectionViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIActionSheetDelegate>
{
    NSMutableArray *pupImgArr;
    NSMutableArray *pupImageArr;
    UICollectionView *collectionView;
    DataClass *dataObj;
    UILabel *message;
    UILabel *pupName;
    UIImageView *pupImage;
    UILabel *ownerName;
    BOOL isReachable;
    UIImagePickerController *pickerimage;
}

@end
static NSString * const PhotoCellIdentifier = @"PhotoCell";
@implementation GalleryViewController
- (void)viewDidLoad {
    [super viewDidLoad];
   
    self.view.backgroundColor=APP_BACKGROUND_COLOR;
    [self.navigationItem setTitle:@"Photo Gallery"];
    dataObj=[DataClass getInstance];
    pupImageArr=[[NSMutableArray alloc]init];
    pupImgArr=[[NSMutableArray alloc]init];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getPupImages) name:@"GET_PUP_IMAGE" object:nil];
    
    [self navigationBarButton];
    
    pickerimage=[[UIImagePickerController alloc]init];
    pickerimage.delegate=self;
    pickerimage.allowsEditing=YES;


    UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc] init];
    collectionView=[[UICollectionView alloc]initWithFrame:CGRectMake(15,20, self.view.frame.size.width-30, self.view.frame.size.height-144) collectionViewLayout:layout];
    [collectionView setDataSource:self];
    [collectionView setDelegate:self];
    [collectionView setBackgroundColor:APP_BACKGROUND_COLOR];
    [self.view addSubview:collectionView];
    [collectionView registerClass:[CollectionViewCell class] forCellWithReuseIdentifier:PhotoCellIdentifier];
    dataObj.updateGallery=@"0";
    
    message=[[UILabel alloc]initWithFrame:CGRectMake(0,200,self.view.frame.size.width, 40)];
    message.textAlignment=NSTextAlignmentCenter;
    message.text=@"No Photos";
    message.textColor=APP_COLOR;
    message.font=ModernicaLargeBold;
    message.numberOfLines = 1;
    message.hidden=YES;
    [self.view addSubview:message];
    
    [self fetchPupImages];
}

-(void)viewWillAppear:(BOOL)animated
{
    
    
    self.navigationController.navigationBarHidden=NO;
    self.navigationItem.hidesBackButton = YES;
    isReachable=[MTReachabilityManager isReachable];
//    if(isReachable && pupImageArr.count==0)
//    {
//    }
    
    if(dataObj.pupObject)
    {
//        pupName.text=[dataObj.pupObject[@"name"] capitalizedString];
        [pupImage setImage:dataObj.pupImage];
       // [ownerName sizeToFit];
    }else{
        message.hidden=NO;
    }
    
    if([dataObj.updateGallery isEqualToString:@"1"])
    {
        [self fetchPupImages];
        dataObj.updateGallery=@"0";
    }


}

-(void)viewWillDisappear:(BOOL)animated
{
    [AppDelegate hideLoading:self.view];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark- UINavigationBar Items
#pragma mark-

-(void)navigationBarButton
{
    UIView *view=[[UIView alloc]initWithFrame:CGRectMake(0,0,70,40)];
    //view.backgroundColor=[UIColor blackColor];
    view.userInteractionEnabled=YES;
    UITapGestureRecognizer *tap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(showPupDetail)];
    tap.numberOfTapsRequired=1;
    [view addGestureRecognizer:tap];
    
    pupImage=[[UIImageView alloc]initWithFrame:CGRectMake(view.frame.size.width-25,0,25,25)];
    // pupImage.layer.borderColor=[UIColor whiteColor].CGColor;
    //pupImage.layer.borderWidth=1;
    pupImage.layer.cornerRadius=5;
    pupImage.layer.masksToBounds=YES;
    [view addSubview:pupImage];
    
    pupName=[[UILabel alloc]initWithFrame:CGRectMake(0,25,70, 15)];
    pupName.textColor=[UIColor whiteColor];
    pupName.textAlignment=NSTextAlignmentRight;
    pupName.font=ModernicaSmallBold;
    [view addSubview:pupName];
    
    UIBarButtonItem *rightBarButtonItem=[[UIBarButtonItem alloc]initWithCustomView:view];
    self.navigationItem.rightBarButtonItem = rightBarButtonItem;
    
    
    UIBarButtonItem *addImage = [[UIBarButtonItem alloc]initWithTitle:@"+" style:UIBarButtonItemStylePlain target:self action:@selector(addImageBtnPressed)];
    
    [addImage setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                    [UIColor whiteColor], NSForegroundColorAttributeName,
                                    [UIFont fontWithName:@"ModernicaBold" size:40.0], NSFontAttributeName, nil]
                          forState:UIControlStateNormal];
    
    self.navigationItem.leftBarButtonItem=addImage;
    
}

-(void)addImageBtnPressed
{
    if(!dataObj.pupObject)
    {
       UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Sorry!" message:@"You have no pup" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }

    UIActionSheet *sheet=[[UIActionSheet alloc]initWithTitle:@"Choose Option" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Camera",@"Gallery" ,nil];
    [sheet showInView:[UIApplication sharedApplication].keyWindow];
    
    
}

#pragma mark- UIActionSheet delegate
#pragma mark-

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex==0)
    {
        if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
        {
            UIAlertView *noCameraAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"You don't have a camera for this device" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [noCameraAlert show];
        }
        else
        {
            pickerimage.sourceType=UIImagePickerControllerSourceTypeCamera;
            
            [self.view.window.rootViewController presentViewController:pickerimage animated:YES completion:nil];
        }
    }
    else if (buttonIndex==1)
    {
        pickerimage.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
        [self.view.window.rootViewController presentViewController:pickerimage animated:YES completion:nil];
        
    }
}

#pragma mark- UIImagePickerController delegate
#pragma mark-

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    FullImageViewController *full=[[FullImageViewController alloc]init];
    UIImage *image=[info objectForKey:UIImagePickerControllerEditedImage];
    image=[AppDelegate imageWithImage:image convertToSize:CGSizeMake(640,640)];
    [picker dismissViewControllerAnimated:YES completion:nil];
    full.selectedImg=image;
    
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:self.navigationItem.backBarButtonItem.style target:nil action:nil];
    
    [self.navigationController pushViewController:full animated:NO];
    
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:NULL];
}


#pragma mark- UICollectionView Delegate
#pragma mark-

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return pupImgArr.count;
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView1 cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    CollectionViewCell *photoCell =[collectionView dequeueReusableCellWithReuseIdentifier:PhotoCellIdentifier
                    forIndexPath:indexPath];
    
    NSString *string = [NSString stringWithFormat:@"%@",pupImgArr[indexPath.row]];

    NSData *data = [[NSData alloc] initWithBase64EncodedString:string options:NSDataBase64DecodingIgnoreUnknownCharacters];
    UIImage *image = [UIImage imageWithData:data];
    NSLog(@"%@",image);
    
    //[myImg setImage:image forState:UIControlStateNormal];
//    NSMutableDictionary *dict=pupImgArr[indexPath.row];
     [photoCell.imageView setImage:image];
    
    return photoCell;
    
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(90,90);
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionView *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 10; // This is the minimum inter item spacing, can be more
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    FullImageView *full=[[FullImageView alloc]init];
    //full.img=img;
    
//    NSString *string = [NSString stringWithFormat:@"%@",pupImgArr[indexPath.row]];
//    
//    NSData *data = [[NSData alloc] initWithBase64EncodedString:string options:NSDataBase64DecodingIgnoreUnknownCharacters];
//    UIImage *image = [UIImage imageWithData:data];
//    NSLog(@"%@",image);
    
    
//    full.pupImgObj=pupImageArr[indexPath.row];
    full.imageArr=pupImgArr;
    full.pupImageObjArr=pupImageArr;
    full.pupName=pupName.text;
    full.isFromGallerry=@"1";
    full.selectedIndex=[NSNumber numberWithInteger:indexPath.row];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:self.navigationItem.backBarButtonItem.style target:nil action:nil];
    
    [self.navigationController pushViewController:full animated:YES];
    
}

#pragma mark- Other Selector
#pragma mark-


-(void)showPupDetail
{
    PFObject *pup=dataObj.pupObject;
    NSLog(@"pupObject=%@",pup);
//    PFUser *pfUser=pup[@"created_by"];
    
//    if([pfUser.objectId isEqualToString:[PFUser currentUser].objectId])
//    {
    
    
    AppDelegate *appDel = (AppDelegate *) [[UIApplication sharedApplication] delegate];
    
    appDel.cameFromListing = YES;
        AddPupViewController *addPup;
        addPup=[[AddPupViewController alloc]init];
        addPup.isEditDeatil=@"1";
        addPup.pupImage=dataObj.pupImage;
//        addPup.pupObjectId=pup.objectId;
        self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:self.navigationItem.backBarButtonItem.style target:nil action:nil];
        [self.navigationController pushViewController:addPup animated:YES];
//
//    }else
//    {
//        PupDetailViewController *pupDeatilObj=[[PupDetailViewController alloc]init];
//        pupDeatilObj.pupImage=dataObj.pupImage;
////        pupDeatilObj.pupObjectId=pup.objectId;
//        pupDeatilObj.pupOwner=ownerName.text;
////        pupDeatilObj.pupObject=pup;
//        self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:self.navigationItem.backBarButtonItem.style target:nil action:nil];
//        [self.navigationController pushViewController:pupDeatilObj animated:YES];
//    }
    
}


-(void)fetchPupImages
{
    [pupImageArr removeAllObjects];
    [pupImgArr removeAllObjects];
    isReachable=[MTReachabilityManager isReachable];
    if(isReachable)
    {
        if(dataObj.pupObject){
            
            
            
            FIRDatabaseReference *firebaseRef= [[FIRDatabase database] reference];
            //            NSString *userID = [FIRAuth auth].currentUser.uid;
            
            [[firebaseRef child:@"AddImage"]
             observeSingleEventOfType:FIRDataEventTypeValue
             withBlock:^(FIRDataSnapshot *snapshot) {
                 // Loop over children
                 
                 NSLog(@"%@",snapshot);
                 NSEnumerator *children = [snapshot children];
                 FIRDataSnapshot *child;
                 while (child = [children nextObject])
                 {
                     NSLog(@"%@",child);
                     NSString *userID = [FIRAuth auth].currentUser.uid;
                     
                     if ([userID isEqual:child.key])
                     {
                         NSEnumerator *children1 = [child children];
                         FIRDataSnapshot *child1;
                         while (child1 = [children1 nextObject])
                         {
                             NSUserDefaults *prefs1 = [NSUserDefaults standardUserDefaults];
                             // getting an NSString
                             NSString *Pupid = [prefs1 stringForKey:@"Pupid"];
                         
                             if ([child1.key isEqual:Pupid])
                             {
                                 NSEnumerator *children2 = [child1 children];
                                 FIRDataSnapshot *child2;
                                 while (child2 = [children2 nextObject])
                                 {


                        
                                 [pupImgArr addObject:child2.value[@"Image"]];
                                 NSLog(@"%@",pupImgArr);
                                 }
                                 
                             }
                         }
                         
                         
                         
                     }
                 }
                 
                 [collectionView reloadData];
             }];

            [collectionView reloadData];
            
            
            
            
            
            
            
            
            
            
            
            
            
//            PFQuery *query = [PFQuery queryWithClassName:@"PupPictures"];
//            [query whereKey:@"pup" equalTo:dataObj.pupObject];
//            [query orderByDescending:@"createdAt"];
//            [AppDelegate showLoading:self.navigationController.view];
//            [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
//                if (!error) {
//                    
//                    // The find succeeded. The first 100 objects are available in objects
//                    NSLog(@"pup Image Count=%lu",(unsigned long)objects.count);
//                    
//                    if(objects.count)
//                    {
//                        message.hidden=YES;
//                        pupImageArr=[NSMutableArray arrayWithArray:[[objects reverseObjectEnumerator] allObjects]];
//                        for(int i=0;i<pupImageArr.count;i++)
//                        {
//                            PFObject *obj=pupImageArr[i];
//                            NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
//                            PFFile* file = obj[@"pic"];
//                            [file getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
//                                if (!error) {
//                                    [dict setObject:data forKey:@"pic"];
//                                    [dict setObject:obj.createdAt forKey:@"createdAt"];
//                                    
//                                    [pupImgArr addObject:dict];
//                                    if(pupImgArr.count==pupImageArr.count)
//                                    {
//                                        [AppDelegate hideLoading:self.navigationController.view];
//                                        [self sortImages];
//                                        [collectionView reloadData];
//                                        
//                                    }
//                                    
//                                }
//                            }];
//                        }
//                    }
//                    else
//                    {
//                        [AppDelegate hideLoading:self.navigationController.view];
//                        message.hidden=NO;
//                        [collectionView reloadData];
//                    }
//                    
//                }else{
//                    // Log details of the failure
//                    [AppDelegate hideLoading:self.navigationController.view];
//                    NSLog(@"Error: %@ %@", error, [error userInfo]);
//                }
//            }];
//        }

    }else
    {
        
        [AppDelegate hideLoading:self.navigationController.view];
        [collectionView reloadData];
        [AppDelegate showNetworkEroor];

    }
   
}
}


-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"GET_PUP_IMAGE" object:nil];
}
-(void)getPupImages
{
    dataObj.updateGallery=@"1";
}
-(void)sortImages
{
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey: @"createdAt" ascending: YES];
    NSArray *sortedArray = [pupImgArr sortedArrayUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    [pupImgArr removeAllObjects];
    pupImgArr=[NSMutableArray arrayWithArray:sortedArray];
    
    
    NSArray *sortedArray2 = [pupImageArr sortedArrayUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    [pupImageArr removeAllObjects];
    pupImageArr=[NSMutableArray arrayWithArray:sortedArray2];
    
}
@end
