//
//  FirtstViewController.m
//  pUpdate
//
//  Created by brst on 6/3/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import "FirtstViewController.h"
#import "SignUpViewController.h"

@interface FirtstViewController ()<UIScrollViewDelegate>


@end

@implementation FirtstViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor=[UIColor grayColor];
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    // getting an NSString
    NSString *email = [prefs stringForKey:@"Email"];
    NSString *password = [prefs stringForKey:@"Password"];
    
    if (email && password >0)
    {
        [self nextScreen];
    }
    else{
        
    }
//    PFUser *currentUser = [PFUser currentUser];

//    if (currentUser) {
//        [self nextScreen];
//    } else {
//        // show the signup or login screen
//    }
     self.navigationController.navigationBarHidden=YES;

    
    
    UIImageView *logoImg=[[UIImageView alloc]initWithFrame:CGRectMake(20, self.view.frame.size.height/2-100, self.view.frame.size.width-30,80)];
    logoImg.image=[UIImage imageNamed:@"frontLogo"];
    [self.view addSubview:logoImg];
    
   
    
    UIButton *getStartedBtn=[[UIButton alloc]initWithFrame:CGRectMake(20,logoImg.frame.origin.y+logoImg.frame.size.height+30, self.view.frame.size.width-40, 40)];
    [getStartedBtn setTitle:@"GET STARTED" forState:UIControlStateNormal];
    getStartedBtn.titleEdgeInsets=UIEdgeInsetsMake(-4, 0, 4, 0);

    getStartedBtn.layer.cornerRadius=5;
    [getStartedBtn addTarget:self action:@selector(getStartedBtnPressed) forControlEvents:UIControlEventTouchUpInside];
    [getStartedBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    getStartedBtn.backgroundColor=APP_COLOR;
    getStartedBtn.titleLabel.font=ModernicaLargestBold;
    
    [self.view addSubview:getStartedBtn];
    
    if(self.view.frame.size.height<568)
        [getStartedBtn setFrame:CGRectMake(20,320, self.view.frame.size.width-40, 40)];


    
}



-(void)getStartedBtnPressed
{
    SignUpViewController *signUp=[[SignUpViewController alloc]init];
    [self.navigationController pushViewController:signUp animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)nextScreen
{
    TabBarViewController *tabObj;
    if(!tabObj)
        tabObj=[[TabBarViewController alloc]init];
    [self.navigationController pushViewController:tabObj animated:NO];
    tabObj=nil;
}
@end
